export default {
  app_greeting: 'Hi from Web!',
  app_title_header: 'Vite + React',
  app_not_found: 'Page not found',
  app_lang_id: 'Indonesian',
  app_lang_en: 'English',
  app_lang_tittle: 'Shortly',
  app_lang_link_1: 'Features',
  app_lang_link_2: 'Pricing',
  app_lang_link_3: 'Resources',
  app_lang_login: 'Login',
  app_lang_signUp: 'Sign up',
  app_lang_banner_tittle: 'More than just shorter links',
  app_lang_banner_text: 'Build your brand’s recognition and get detailed insights on how your links are performing.',
  app_lang_btn_started: 'Get Started',
  app_lang_btn_shorten: 'Shorten it!',
  app_lang_btn_delete: 'delete',
  app_lang_advice_title: 'Advanced Statics',
  app_lang_advice_text: 'Track how your links are performing across the web with our advanced statistics dashboard.',
  app_lang_card_recognition_tittle: 'Brand Recognition',
  app_lang_card_recognition_text: `Boost your brand recognition with each click.Generic links don't mean a thing. Branded links help intstil confidence in your content.`,
  app_lang_card_records_tittle: 'Detailed Records',
  app_lang_card_records_text:
    'Gain insight into who is clicking your links.Knowing when and where people engage with your content helps inform better decisions.',
  app_lang_card_customizable_tittle: 'Fully Customizable',
  app_lang_card_customizable_text:
    'Improve brand awareness and content discoverability through customizable links, supercharging audience engagement.',
  app_lang_bannerBtm_tittle: 'Boost your links today',
  app_lang_footer_features_1: 'Link Shortening',
  app_lang_footer_features_2: 'Branded Links',
  app_lang_footer_features_3: 'Analytics',
  app_lang_footer_resources_1: 'Blog',
  app_lang_footer_resources_2: 'Developers',
  app_lang_footer_resources_3: 'Supports',
  app_lang_footer_company_1: 'About',
  app_lang_footer_company_2: 'Our Team',
  app_lang_footer_company_3: 'Carrers',
  app_lang_footer_company_4: 'Contact',
  app_lang_btn_copy: 'Copy',
  app_lang_btn_copied: 'Copied!',
  app_lang_placeholder: 'Shorten a link here...',
  app_lang_err1: 'No URL specified ("url" parameter is empty)',
  app_lang_err2: 'Invalid URL submitted',
  app_lang_err3: 'Rate limit reached. Wait a second and try again',
  app_lang_err4: 'IP-Address has been blocked because of violating our terms of service',
  app_lang_err5: 'shrtcode code (slug) already taken/in use',
  app_lang_err6: 'Unknown error',
  app_lang_err7: 'No code specified ("code" parameter is empty)',
  app_lang_err8: 'Invalid code submitted (code not found/there is no such short-link)',
  app_lang_err9: 'Missing required parameters',
  app_lang_err10: 'Trying to shorten a disallowed Link. More information on disallowed links',
  app_lang_err11: 'Url is already exist',
  app_lang_err_null: 'Invalid URL',
  app_lang_err_link_exists: 'This link already exists',
  app_lang_delete_confirmation_title: 'Delete Link',
  app_lang_delete_confirmation_text: 'Are you sure you want to delete this link?',
  app_lang_delete_confirmation_confirm: 'Yes, delete it!',
  app_lang_delete_confirmation_cancel: 'Cancel',
};
