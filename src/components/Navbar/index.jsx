import PropTypes from 'prop-types';
import { useState } from 'react';
import { FormattedMessage } from 'react-intl';
import { setLocale } from '@containers/App/actions';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import AppBar from '@mui/material/AppBar';
import Avatar from '@mui/material/Avatar';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import MenuRoundedIcon from '@mui/icons-material/MenuRounded';
import CloseIcon from '@mui/icons-material/Close';
import FlagId from '@static/images/flags/id.png';
import FlagEn from '@static/images/flags/en.png';

import classes from './style.module.scss';

const Navbar = ({ title, locale, contentRef }) => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [menuPosition, setMenuPosition] = useState(null);
  const [isMobileNavbarOpen, setIsMobileNavbarOpen] = useState(false);

  const open = Boolean(menuPosition);

  const handleClick = (event) => {
    setMenuPosition(event.currentTarget);
    document.body.style.overflow = 'auto';
  };

  const handleClose = () => {
    setMenuPosition(null);
  };

  const toggleMobileNavbar = () => {
    setIsMobileNavbarOpen((prevState) => !prevState);
    if (!isMobileNavbarOpen) {
      document.body.style.overflow = 'hidden';
    } else {
      document.body.style.overflow = 'auto';
    }
  };

  const onSelectLang = (lang) => {
    if (lang !== locale) {
      dispatch(setLocale(lang));
    }
    handleClose();
  };

  const goHome = () => {
    navigate('/');
  };
  const handleLinkClick = (targetId) => {
    const targetElement = document.getElementById(targetId);
    if (targetElement) {
      targetElement.scrollIntoView({ behavior: 'smooth' });
      document.body.style.overflow = 'auto';
    }
    setIsMobileNavbarOpen(false);
  };

  return (
    <>
      <AppBar className={`${classes.headerWrapper} ${isMobileNavbarOpen ? 'active' : ''}`} ref={contentRef}>
        <div className={classes.contentWrapper}>
          <div className={classes.logoImage} onClick={goHome}>
            <div className={classes.title}>{title}</div>
            <div className={classes.box_link}>
              <div className={classes.link} onClick={() => handleLinkClick('banner')}>
                <FormattedMessage id="app_lang_link_1" />
              </div>
              <div className={classes.link} onClick={() => handleLinkClick('sorting')}>
                <FormattedMessage id="app_lang_link_2" />
              </div>
              <div className={classes.link} onClick={() => handleLinkClick('advance')}>
                <FormattedMessage id="app_lang_link_3" />
              </div>
            </div>
          </div>
          {/* Toggle button for mobile navbar */}
          <div className={`${classes.menu_toggle} ${isMobileNavbarOpen ? 'active' : ''}`} onClick={toggleMobileNavbar}>
            {isMobileNavbarOpen ? <CloseIcon /> : <MenuRoundedIcon />}
          </div>
          <div className={classes.utils_navbar}>
            <div className={classes.toolbar}>
              <div className={classes.box_auth}>
                <button type="button" className={classes.login}>
                  Login
                </button>
                <button type="button" className={classes.signUp}>
                  Sign Up
                </button>
              </div>
              <div className={classes.toggle} onClick={handleClick}>
                <Avatar className={classes.avatar} src={locale === 'id' ? FlagId : FlagEn} />
                <div className={classes.lang}>{locale}</div>
                <ExpandMoreIcon />
              </div>
            </div>
            <Menu open={open} anchorEl={menuPosition} onClose={handleClose}>
              <MenuItem onClick={() => onSelectLang('id')} selected={locale === 'id'}>
                <div className={classes.menu}>
                  <Avatar className={classes.menuAvatar} src={FlagId} />
                  <div className={classes.menuLang}>
                    <FormattedMessage id="app_lang_id" />
                  </div>
                </div>
              </MenuItem>
              <MenuItem onClick={() => onSelectLang('en')} selected={locale === 'en'}>
                <div className={classes.menu}>
                  <Avatar className={classes.menuAvatar} src={FlagEn} />
                  <div className={classes.menuLang}>
                    <FormattedMessage id="app_lang_en" />
                  </div>
                </div>
              </MenuItem>
            </Menu>
          </div>
        </div>
      </AppBar>

      {/* Mobile Navbar */}
      <div
        className={`${classes.headerWrapperMobile} ${
          isMobileNavbarOpen ? classes.mobileNavbarEnterActive : classes.mobileNavbarExitActive
        }`}
      >
        {isMobileNavbarOpen && (
          <div className={`${classes.contentWrapperMobile} ${classes.mobileNavbarEnter}`}>
            <div className={classes.menu_link_mobile}>
              <div onClick={() => handleLinkClick('banner')}>
                <FormattedMessage id="app_lang_link_1" />
              </div>
              <div onClick={() => handleLinkClick('sorting')}>
                <FormattedMessage id="app_lang_link_2" />
              </div>
              <div onClick={() => handleLinkClick('advance')}>
                <FormattedMessage id="app_lang_link_3" />
              </div>
              <div className={classes.toggle} onClick={handleClick}>
                <div className={classes.lang}>{locale}</div>
                <ExpandMoreIcon />
              </div>
            </div>
            <div className={classes.login_signup_mobile}>
              <div>
                <button className={classes.login} type="button">
                  Login
                </button>
              </div>
              <div>
                <button className={classes.signUp} type="button">
                  Sign Up
                </button>
              </div>
            </div>
          </div>
        )}
      </div>
    </>
  );
};

Navbar.propTypes = {
  title: PropTypes.string,
  locale: PropTypes.string.isRequired,
  contentRef: PropTypes.object,
};

export default Navbar;
