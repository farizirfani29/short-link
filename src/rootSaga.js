import { all } from 'redux-saga/effects';

import appSaga from '@containers/App/saga';
import ShortUrlSaga from '@containers/ShortingLink/saga';

export default function* rootSaga() {
  yield all([appSaga(), ShortUrlSaga()]);
}
