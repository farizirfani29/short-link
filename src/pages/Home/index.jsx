import AdvanceComp from './components/AdvanceComp';
import Banner from './components/BannerComp';
import BannerBtmComp from './components/BannerBtmComp';
import Sorting from './components/SortingComp';
import scss from './style.module.scss';
import Footer from './components/Footer';

const Home = () => (
  <div className={scss.container}>
    <div className={scss.banner}>
      <Banner id="banner" />
    </div>
    <div className={scss.sort_static}>
      <Sorting id="sorting" />
      <AdvanceComp id="advance" />
    </div>
    <div className={scss.BannerBtm}>
      <BannerBtmComp id="bannerBtm" />
    </div>
    <div className={scss.footer}>
      <Footer id="footer" />
    </div>
  </div>
);

export default Home;
