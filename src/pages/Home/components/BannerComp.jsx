/* eslint-disable react/prop-types */
import { FormattedMessage } from 'react-intl';
import imgBanner from '@static/images/images/illustration-working.svg';
import scss from '../style/banner.module.scss';

const Banner = ({ id }) => (
  <div className={scss.container} id={id}>
    <div className={scss.content}>
      <div className={scss.banner}>
        <div className={scss.banner_right}>
          <div className={scss.title}>
            <FormattedMessage id="app_lang_banner_tittle" />
          </div>
          <div className={scss.text}>
            <FormattedMessage id="app_lang_banner_text" />
          </div>
          <div className={scss.box_btn}>
            <button type="button" className={scss.button}>
              <FormattedMessage id="app_lang_btn_started" />
            </button>
          </div>
        </div>
        <div className={scss.banner_left}>
          <img src={imgBanner} width="100px" alt="" />
        </div>
      </div>
    </div>
  </div>
);

export default Banner;
