/* eslint-disable react/prop-types */
import PropTypes from 'prop-types';
import { FormattedMessage, injectIntl } from 'react-intl';
import { useDispatch, useSelector } from 'react-redux';
import { getShortUrl, setDeleteShortUrl, setShortUrlError } from '@containers/ShortingLink/actions';
import { useState } from 'react';
import Swal from 'sweetalert2';
import CopyToClipboard from 'react-copy-to-clipboard';
import scss from '../style/sorting.module.scss';

const SortingComp = ({ intl, id }) => {
  const dispatch = useDispatch();
  const resultLink = useSelector((state) => state.shortingLink);
  const resultError = resultLink.shortUrlError;
  const resultLoading = resultLink.shortUrlLoading;
  const [inputValue, setInputValue] = useState('');
  const [copiedStatus, setCopiedStatus] = useState({});
  let MessageError = '';

  const handleShortenClick = (event) => {
    event.preventDefault();
    const isLinkExists = resultLink.shortUrl.some((link) => link.original_link === inputValue);

    if (isLinkExists) {
      MessageError = intl.formatMessage({ id: 'app_lang_err_link_exists' });
      dispatch(setShortUrlError({ errorCode: 11 }));
    } else {
      dispatch(getShortUrl(inputValue));
      setInputValue('');
      MessageError = '';
    }
  };

  const handleInputChange = (event) => {
    setInputValue(event.target.value);
    if (resultError) {
      dispatch(setShortUrlError(null));
    }
  };

  const handleCopyLink = (shortLink) => {
    setCopiedStatus((prevStatus) => ({
      ...prevStatus,
      [shortLink]: true,
    }));

    setTimeout(() => {
      setCopiedStatus((prevStatus) => ({
        ...prevStatus,
        [shortLink]: false,
      }));
    }, 3000);
  };

  // handle error
  if (resultError) {
    switch (resultError.errorCode) {
      case 1:
        MessageError = intl.formatMessage({ id: 'app_lang_err1' });
        break;
      case 2:
        MessageError = intl.formatMessage({ id: 'app_lang_err2' });
        break;
      case 3:
        MessageError = intl.formatMessage({ id: 'app_lang_err3' });
        break;
      case 4:
        MessageError = intl.formatMessage({ id: 'app_lang_err4' });
        break;
      case 5:
        MessageError = intl.formatMessage({ id: 'app_lang_err5' });
        break;
      case 6:
        MessageError = intl.formatMessage({ id: 'app_lang_err6' });
        break;
      case 7:
        MessageError = intl.formatMessage({ id: 'app_lang_err7' });
        break;
      case 8:
        MessageError = intl.formatMessage({ id: 'app_lang_err8' });
        break;
      case 9:
        MessageError = intl.formatMessage({ id: 'app_lang_err9' });
        break;
      case 10:
        MessageError = intl.formatMessage({ id: 'app_lang_err10' });
        break;
      case 11:
        MessageError = intl.formatMessage({ id: 'app_lang_err11' });
        break;
      case null:
        MessageError = intl.formatMessage({ id: 'app_lang_err_null  ' });
        break;
      default:
        MessageError = 'Error';
        break;
    }
  }

  const handleDeleteLink = (shortLink) => {
    Swal.fire({
      title: intl.formatMessage({ id: 'app_lang_delete_confirmation_title' }),
      text: intl.formatMessage({ id: 'app_lang_delete_confirmation_text' }),
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: intl.formatMessage({ id: 'app_lang_delete_confirmation_confirm' }),
      cancelButtonText: intl.formatMessage({ id: 'app_lang_delete_confirmation_cancel' }),
    }).then((result) => {
      if (result.isConfirmed) {
        dispatch(setDeleteShortUrl(shortLink));
      }
    });
  };

  return (
    <div id={id} className={scss.container}>
      <div className={scss.content}>
        <form onSubmit={handleShortenClick} className={scss.box_sorting}>
          <div className={scss.errorHandle}>
            <input
              type="url"
              value={inputValue}
              onChange={handleInputChange}
              className={`${resultError !== null ? scss.errorInput : scss.input}`}
              placeholder={intl.formatMessage({ id: 'app_lang_placeholder' })}
              required
              disabled={resultLoading}
            />
            {resultError !== null ? <div className={scss.errorMobile}>{MessageError}</div> : null}
          </div>
          <button type="button" disabled={resultLoading} className={scss.button}>
            <FormattedMessage id="app_lang_btn_shorten" />
          </button>
          {resultError !== null ? <div className={scss.error}>{MessageError}</div> : null}
        </form>
      </div>
      {resultLoading === true ? (
        <div className={scss.spinner} />
      ) : (
        resultLink.shortUrl?.map((result, index) => (
          <div className={scss.box_sorted} key={index}>
            <div className={scss.result_link}>
              <p className={scss.ori_link}>{result.original_link}</p>
              <div className={scss.box_link_btn}>
                <p className={scss.short_link}>{result.short_link}</p>
                <CopyToClipboard text={result.short_link} onCopy={() => handleCopyLink(result.short_link)}>
                  <button
                    type="button"
                    className={`${scss.button} ${copiedStatus[result.short_link] ? scss.copyButtonCopied : ''}`}
                  >
                    {copiedStatus[result.short_link] ? (
                      <FormattedMessage id="app_lang_btn_copied" />
                    ) : (
                      <FormattedMessage id="app_lang_btn_copy" />
                    )}
                  </button>
                </CopyToClipboard>
                <button type="button" className={scss.deleteButton} onClick={() => handleDeleteLink(result.short_link)}>
                  <FormattedMessage id="app_lang_btn_delete" />
                </button>
              </div>
            </div>
          </div>
        ))
      )}
    </div>
  );
};

SortingComp.propTypes = {
  intl: PropTypes.object,
};

export default injectIntl(SortingComp);
