/* eslint-disable react/prop-types */
import { FormattedMessage } from 'react-intl';
import scss from '../style/bannerbtm.module.scss';

const BannerBtmComp = ({ id }) => (
  <div id={id} className={scss.container}>
    <div className={scss.content}>
      <div className={scss.text}>
        <FormattedMessage id="app_lang_bannerBtm_tittle" />
      </div>
      <div className={scss.box_card}>
        <button type="button" className={scss.button}>
          <FormattedMessage id="app_lang_btn_started" />
        </button>
      </div>
    </div>
  </div>
);

export default BannerBtmComp;
