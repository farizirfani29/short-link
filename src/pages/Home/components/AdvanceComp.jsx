/* eslint-disable no-undef */
/* eslint-disable react/prop-types */

import { FormattedMessage } from 'react-intl';
import Recognition from '@static/images/images/icon-brand-recognition.svg';
import Records from '@static/images/images/icon-detailed-records.svg';
import Customizable from '@static/images/images/icon-fully-customizable.svg';
import scss from '../style/advance.module.scss';

const AdvanceComp = ({ id }) => (
  <div id={id} className={scss.container}>
    <div className={scss.content}>
      <div className={scss.box_text}>
        <div className={scss.title}>
          <FormattedMessage id="app_lang_advice_title" />
        </div>
        <div className={scss.text}>
          <FormattedMessage id="app_lang_advice_text" />
        </div>
      </div>
    </div>
    <div className={scss.box_card}>
      <Card
        CardStyle={scss.card1}
        Image={Recognition}
        Title={<FormattedMessage id="app_lang_card_recognition_tittle" />}
        Text={<FormattedMessage id="app_lang_card_recognition_text" />}
      />
      <Card
        CardStyle={scss.card2}
        Image={Records}
        Title={<FormattedMessage id="app_lang_card_records_tittle" />}
        Text={<FormattedMessage id="app_lang_card_records_text" />}
      />
      <Card
        CardStyle={scss.card3}
        Image={Customizable}
        Title={<FormattedMessage id="app_lang_card_customizable_tittle" />}
        Text={<FormattedMessage id="app_lang_card_customizable_text" />}
      />
    </div>
  </div>
);

const Card = (props) => {
  const { Title, Text, CardStyle, Image } = props;
  return (
    <div className={`${CardStyle} ${scss.card}`}>
      <div className={scss.card_img}>
        <img className={scss.img} width="100%" height="100%" src={Image} alt="" />
      </div>
      <div className={scss.card_content}>
        <div className={scss.card_title}>{Title}</div>
        <div className={scss.card_text}>{Text}</div>
      </div>
    </div>
  );
};

export default AdvanceComp;
