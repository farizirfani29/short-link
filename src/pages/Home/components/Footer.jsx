/* eslint-disable react/prop-types */
import { FormattedMessage } from 'react-intl';
import Facebook from '@static/images/images/icon-facebook.svg';
import Instagram from '@static/images/images/icon-instagram.svg';
import Pinterest from '@static/images/images/icon-pinterest.svg';
import Twitter from '@static/images/images/icon-twitter.svg';
import scss from '../style/footer.module.scss';

const Footer = ({ id }) => (
  <div id={id} className={scss.container}>
    <div className={scss.content}>
      <div className={scss.logo}>
        <h2>
          <FormattedMessage id="app_lang_tittle" />
        </h2>
      </div>
      <div className={scss.features}>
        <p className={scss.title}>
          <FormattedMessage id="app_lang_link_1" />
        </p>
        <p>
          <FormattedMessage id="app_lang_footer_features_1" />
        </p>
        <p>
          <FormattedMessage id="app_lang_footer_features_2" />
        </p>
        <p>
          <FormattedMessage id="app_lang_footer_features_3" />
        </p>
      </div>
      <div className={scss.resources}>
        <p className={scss.title}>
          <FormattedMessage id="app_lang_link_2" />
        </p>
        <p>
          <FormattedMessage id="app_lang_footer_resources_1" />
        </p>
        <p>
          <FormattedMessage id="app_lang_footer_resources_2" />
        </p>
        <p>
          <FormattedMessage id="app_lang_footer_resources_3" />
        </p>
      </div>
      <div className={scss.company}>
        <p className={scss.title}>company</p>
        <p>
          <FormattedMessage id="app_lang_footer_company_1" />
        </p>
        <p>
          <FormattedMessage id="app_lang_footer_company_2" />
        </p>
        <p>
          <FormattedMessage id="app_lang_footer_company_3" />
        </p>
        <p>
          <FormattedMessage id="app_lang_footer_company_4" />
        </p>
      </div>
      <div className={scss.medsos}>
        <img src={Facebook} alt="Facebook" />
        <img src={Twitter} alt="Twitter" />
        <img src={Pinterest} alt="Pinterest" />
        <img src={Instagram} alt="Instagram" />
      </div>
    </div>
  </div>
);

export default Footer;
