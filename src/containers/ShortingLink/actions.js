import {
  GET_SHORT_URL,
  SET_SHORT_URL,
  SET_SHORT_URL_ERROR,
  SET_SHORT_URL_LOADING,
  DELETE_SHORT_URL,
} from '@containers/ShortingLink/constants';

export const setDeleteShortUrl = (deleteShortUrl) => ({
  type: DELETE_SHORT_URL,
  deleteShortUrl,
});
export const getShortUrl = (url) => ({
  type: GET_SHORT_URL,
  url,
});

export const setShortUrl = (shortUrl) => ({
  type: SET_SHORT_URL,
  shortUrl,
});

export const setShortUrlLoading = (shortUrlLoading) => ({
  type: SET_SHORT_URL_LOADING,
  shortUrlLoading,
});

export const setShortUrlError = (shortUrlError) => ({
  type: SET_SHORT_URL_ERROR,
  shortUrlError,
});
