export const GET_SHORT_URL = 'ShortingLink/GET_SHORT_URL';
export const SET_SHORT_URL = 'ShortingLink/SET_SHORT_URL';
export const SET_SHORT_URL_LOADING = 'ShortingLink/SET_SHORT_URL_LOADING';
export const SET_SHORT_URL_ERROR = 'ShortingLink/SET_SHORT_URL_ERROR';
export const DELETE_SHORT_URL = 'ShortingLink/DELETE_SHORT_URL';
